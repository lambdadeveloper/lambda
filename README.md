# λ::lambda

λ::lambda was designed to be a micro and modular framework in order to use as you need.


## Installation

Install using composer run in your terminal the command `composer create-project php-developer/lambda your-project-name` or simply clone the project from `https://gitlab.com/php-developer/lambda`.



# License and terms of use

The author is not liable for misuse and/or damage caused by free use and/or distribution of this tool.

## Creative Commons 4.0

(cc) 2017, λ::lambda - John Murowaniecki. Some rights reserved.
