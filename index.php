<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * Demo routing file.
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

require_once __DIR__.'/vendor/autoload.php';

(new \CORE\λ($_SERVER['REQUEST_URI']))
    ->route('/about', new \HOLD\APP\Controllers\Landing('about'))
    ->route('/',      new \HOLD\APP\Controllers\Landing('index'));
