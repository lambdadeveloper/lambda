<?php
/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/

namespace APP\Controllers;

/**
 * PHP λ::lambda(); // Personal PHP7 Framework
 *
 * Demo landing page.
 *
 * PHP version 7
 *
 * @category Framework
 * @package  Lambda
 * @author   John Murowaniecki <jmurowaniecki@gmail.com>
 * @license  Creative Commons 4.0 - Some rights reserved.
 * @link     https://gitlab.com/php-developer/lambda
 **/
class Landing extends \CORE\CONTROLLER
{
    /**
     * [__toString description]
     * @return string [description]
     */
    public function __toString()
    {
        return get_class($this);
    }

    /**
     * [index description]
     * @return [type] [description]
     */
    public function index()
    {
        echo "λ Rendered $this";
    }

    /**
     * [about description]
     * @return [type] [description]
     */
    public function about()
    {
        echo "Designed by John Murowaniecki.";
    }
}
